gnome-color-manager (3.36.0-1) unstable; urgency=medium

  * New upstream release

 -- Jeremy Bicha <jbicha@debian.org>  Thu, 02 Apr 2020 22:21:48 -0400

gnome-color-manager (3.35.90-1) unstable; urgency=medium

  [ Helmut Grohne ]
  * Fix FTBFS with nocheck profile. (Closes: #914832)

  [ Laurent Bigonville ]
  * New upstream release
    - debian/control.in: Adjust the (build-)dependencies
    - debian/patches/01_unity_control_center.patch: Dropped, doesn't seem
      needed anymore
  * debian/control.in: Bump Standards-Version to 4.5.0 (no further changes)
  * debian/control.in: Set Rules-Requires-Root: no

 -- Laurent Bigonville <bigon@debian.org>  Sat, 14 Mar 2020 00:57:12 +0100

gnome-color-manager (3.32.0-2) unstable; urgency=medium

  * Release to unstable

 -- Jeremy Bicha <jbicha@debian.org>  Thu, 19 Sep 2019 20:26:39 -0400

gnome-color-manager (3.32.0-1) experimental; urgency=medium

  * New upstream release
  * Bump minimum meson to 0.46.0
  * Build-Depend on debhelper-compat 12 and drop debian/compat
  * Build-Depend on dh-sequence-gnome
  * Stop overriding libexecdir

 -- Jeremy Bicha <jbicha@debian.org>  Fri, 08 Mar 2019 10:05:05 -0500

gnome-color-manager (3.30.0-2) unstable; urgency=medium

  * Restore -Wl,-O1 to our LDFLAGS
  * Bump Standards-Version to 4.3.0

 -- Jeremy Bicha <jbicha@debian.org>  Sun, 23 Dec 2018 10:59:17 -0500

gnome-color-manager (3.30.0-1) unstable; urgency=medium

  * New upstream release
  * debian/docs: Add COMMITMENT (GPL Common Cure Rights Commitment)
  * Add X-Ubuntu-Use-Langpack to opt in to Ubuntu language pack handling
    (LP: #1779574)
  * Bump Standards-Version to 4.2.1

 -- Jeremy Bicha <jbicha@debian.org>  Tue, 04 Sep 2018 20:31:39 -0400

gnome-color-manager (3.28.0-1) unstable; urgency=medium

  * New upstream release
  * Drop 0002-build-Do-not-hardcode-installation-paths.patch: Applied

 -- Jeremy Bicha <jbicha@debian.org>  Mon, 12 Mar 2018 09:56:49 -0400

gnome-color-manager (3.26.0-2) unstable; urgency=medium

  * Update Vcs fields for migration to https://salsa.debian.org/
  * Add debian/gbp.conf
  * Bump Standards-Version to 4.1.3
  * Bump debhelper compat to 11
  * Use appstream-util appdata-to-news to install NEWS
  * Run build tests with dh_auto_test instead of autopkgtest
  * Cherry-pick 0002-build-Do-not-hardcode-installation-paths.patch
    to fix libexecdir override
  * Set libexecdir to /usr/lib/gnome-color-manager

 -- Jeremy Bicha <jbicha@debian.org>  Mon, 26 Feb 2018 22:45:45 -0500

gnome-color-manager (3.26.0-1) unstable; urgency=medium

  * New upstream release
  * Add 01_unity_control_center.patch:
    - Point link to unity-control-center when running Unity

 -- Jeremy Bicha <jbicha@debian.org>  Sat, 16 Sep 2017 18:47:07 -0400

gnome-color-manager (3.25.90-1) unstable; urgency=medium

  * New upstream release
  * Build with meson
  * Bump Standards-Version to 4.1.0

 -- Jeremy Bicha <jbicha@debian.org>  Wed, 30 Aug 2017 13:35:31 -0400

gnome-color-manager (3.22.2-1) unstable; urgency=medium

  * New upstream release.

 -- Michael Biebl <biebl@debian.org>  Tue, 08 Nov 2016 00:22:55 +0100

gnome-color-manager (3.22.1-1) unstable; urgency=medium

  * New upstream release.

 -- Michael Biebl <biebl@debian.org>  Wed, 12 Oct 2016 15:31:00 +0200

gnome-color-manager (3.22.0-1) unstable; urgency=medium

  * New upstream release.

 -- Andreas Henriksson <andreas@fatal.se>  Mon, 19 Sep 2016 20:55:59 +0200

gnome-color-manager (3.21.92-1) unstable; urgency=medium

  * New upstream development release.
    - Fixes man pages to use the correct package name. (Closes: #700241)

 -- Michael Biebl <biebl@debian.org>  Tue, 13 Sep 2016 18:48:47 +0200

gnome-color-manager (3.21.91-1) unstable; urgency=medium

  [ Jeremy Bicha ]
  * New upstream release
  * Convert from cdbs to dh
  * Bump debhelper compat to 10
  * debian/control.in:
    - Build-depend on appstream-util and autoconf-archive
  * Add debian/docs to install NEWS and README
  * Add autopkgtest to run upstream tests

 -- Michael Biebl <biebl@debian.org>  Tue, 30 Aug 2016 17:11:10 +0200

gnome-color-manager (3.20.0-1) unstable; urgency=medium

  * New upstream release.
  * Bump Build-Depends on libcolord-dev and Depends on colord to (>= 1.3.1) as
    per configure.ac.
  * Bump Standards-Version to 3.9.8.

 -- Michael Biebl <biebl@debian.org>  Thu, 21 Apr 2016 00:06:49 +0200

gnome-color-manager (3.18.0-1) unstable; urgency=medium

  * New upstream release.

 -- Andreas Henriksson <andreas@fatal.se>  Sat, 03 Oct 2015 11:09:36 +0200

gnome-color-manager (3.16.0-1) unstable; urgency=medium

  * New upstream release.
  * Bump debhelper compatibility level to 9.

 -- Michael Biebl <biebl@debian.org>  Sat, 30 May 2015 19:24:09 +0200

gnome-color-manager (3.14.1-1) unstable; urgency=medium

  * New upstream release.
  * Point Homepage to the upstream git repository as there is no real project
    website now.
  * Bump Standards-Version to 3.9.6.

 -- Michael Biebl <biebl@debian.org>  Mon, 13 Oct 2014 18:22:34 +0200

gnome-color-manager (3.14.0-1) unstable; urgency=medium

  [ Jackson Doak ]
  * New upstream release
  * debian/rules: enable parallel build. Closes: #759118
  * debian/control: Use vte2.91 instead of vte2.90

 -- Andreas Henriksson <andreas@fatal.se>  Fri, 26 Sep 2014 10:49:43 +0200

gnome-color-manager (3.12.2-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 3.9.5

 -- Andreas Henriksson <andreas@fatal.se>  Fri, 16 May 2014 19:33:46 +0200

gnome-color-manager (3.12.1-1) unstable; urgency=medium

  * Team upload.

  [ Jackson Doak ]
  * New upstream release

 -- Andreas Henriksson <andreas@fatal.se>  Sat, 26 Apr 2014 16:51:46 +0200

gnome-color-manager (3.12.0-1) unstable; urgency=low

  [ Jean Schurger ]
  * New upstream release (3.10.1).
  * Update 'libcolord-dev' and 'colord' dependency according to configure.ac.

  [ Andreas Henriksson ]
  * New upstream release (3.12.0).
  * Update build-dependencies according to configure.ac changes:
    - Drop unused libxrandr-dev, libx11-dev and libgnome-desktop-3-dev.

 -- Andreas Henriksson <andreas@fatal.se>  Mon, 24 Mar 2014 21:50:37 +0100

gnome-color-manager (3.8.3-1) unstable; urgency=low

  * New upstream release.

 -- Michael Biebl <biebl@debian.org>  Sat, 21 Sep 2013 20:59:17 +0200

gnome-color-manager (3.8.2-1) unstable; urgency=low

  [ Jeremy Bicha ]
  * New upstream release.
  * debian/control.in:
    - Build-depend on libtiff-dev instead of libtiff4-dev

  [ Michael Biebl ]
  * Upload to unstable.
  * Bump Standards-Version to 3.9.4. No further changes.

 -- Michael Biebl <biebl@debian.org>  Thu, 27 Jun 2013 03:15:15 +0200

gnome-color-manager (3.8.0-1) experimental; urgency=low

  * New upstream release.
  * Bump build-dependencies according to configure.ac:
    - libcolord-dev (>= 0.1.28)
    - libcolord-gtk-dev (>= 0.1.20)
  * Drop build-dependency on gnome-doc-utils and itstool in favor
    of yelp-tools.

 -- Andreas Henriksson <andreas@fatal.se>  Fri, 12 Apr 2013 23:07:01 +0200

gnome-color-manager (3.6.0-1) experimental; urgency=low

  [ Jeremy Bicha ]
  * debian/control.in: Drop scrollkeeper build-depends

  [ Sjoerd Simons ]
  * New upstream release
  * debian/control.in: Build-depend on libcolord-gtk-dev
  * debian/control.in: Build-depend on itstool

 -- Sjoerd Simons <sjoerd@debian.org>  Sat, 03 Nov 2012 00:35:53 +0100

gnome-color-manager (3.4.2-1) unstable; urgency=low

  * New upstream release.

 -- Michael Biebl <biebl@debian.org>  Mon, 14 May 2012 23:22:58 +0200

gnome-color-manager (3.4.0-2) unstable; urgency=low

  * Upload to unstable.

 -- Michael Biebl <biebl@debian.org>  Thu, 19 Apr 2012 04:14:09 +0200

gnome-color-manager (3.4.0-1) experimental; urgency=low

  * New upstream release.
  * debian/patches/use-gsd-not-gpm-for-brightness.patch: Removed, merged
    upstream.
  * debian/rules: Remove cleanup rule for the stray D-Bus service file. Fixed
    by the new upstream release.
  * Bump Build-Depends on libglib2.0-dev and libgtk-3-dev.
  * Add Build-Depends on libgnome-desktop-3-dev.
  * Update debian/copyright, use the final copyright format 1.0 spec.
  * Update Vcs-* URLs.
  * Bump Standards-Version to 3.9.3.

 -- Michael Biebl <biebl@debian.org>  Mon, 02 Apr 2012 06:06:10 +0200

gnome-color-manager (3.2.2-2) unstable; urgency=low

  * debian/patches/use-gsd-not-gpm-for-brightness.patch
    + Added, Use gnome-settings-daemon for brightness control when calibrating

 -- Sjoerd Simons <sjoerd@debian.org>  Sat, 14 Jan 2012 17:38:06 +0100

gnome-color-manager (3.2.2-1) unstable; urgency=low

  [ Josselin Mouette ]
  * Update repository URL.

  [ Michael Biebl ]
  * New upstream release.
  * Remove org.gnome.ColorManager.service D-Bus service file which references
    the no longer existing gcm-session binary.

 -- Michael Biebl <biebl@debian.org>  Mon, 09 Jan 2012 18:16:15 +0100

gnome-color-manager (3.2.1-2) unstable; urgency=low

  * Upload to unstable.
  * debian/control.in:
    - Add Build-Depends on docbook for the 4.1 DocBook SGML DTD which is
      required to generate the man pages,

 -- Michael Biebl <biebl@debian.org>  Sat, 19 Nov 2011 16:10:29 +0100

gnome-color-manager (3.2.1-1) experimental; urgency=low

  * New upstream release.
  * debian/watch:
    - Track .xz tarballs.
  * debian/patches/01-libnotify_0.7.patch:
    - Removed, fixed upstream.
  * debian/control.in:
    - Drop Build-Depends on autoconf, automake, libtool, libgconf2-dev,
      libgnome-desktop-dev, libgudev-1.0-dev, libdbus-glib-1-dev,
      gtk-doc-tools, libxxf86vm-dev, libunique-dev, libcups2-dev,
      libnotify-dev and libsane-dev.
    - Add Build-Depends on libcolord-dev (>= 0.1.12).
    - Add Build-Depends on libexiv2-dev and libexif-dev.
    - Bump Build-Depends on libglib2.0-dev to (>= 2.30.0).
    - Update Build-Depends on libgtk2.0-dev to libgtk-3-dev (>= 2.91.0).
    - Update Build-Depends on libvte-dev to libvte-2.90-dev (>= 0.25.1).
    - Update Build-Depends on liblcms1-dev to liblcms2-dev (>= 2.2).
    - Update Build-Depends on libcanberra-gtk-dev to libcanberra-gtk3-dev.
    - Drop hwdata Recommends.
    - Add Depends on colord (>= 0.1.12).

 -- Michael Biebl <biebl@debian.org>  Wed, 26 Oct 2011 11:12:21 +0200

gnome-color-manager (2.32.0-2) unstable; urgency=low

  * debian/watch: Track .bz2 tarballs.
  * Bump debhelper compatibility level to 8.
  * Update for libnotify 0.7. (Closes: #630272)
    - Bump Build-Depends on libnotify-dev to (>= 0.7.0).
    - Add debian/patches/01-libnotify_0.7.patch.

 -- Michael Biebl <biebl@debian.org>  Tue, 02 Aug 2011 04:53:11 +0200

gnome-color-manager (2.32.0-1) unstable; urgency=low

  [ Emilio Pozuelo Monfort ]
  * debian/control.in,
    debian/rules:
    - Switch to CDBS.

  [ Michael Biebl ]
  * New upstream release.
    - Fixes FTBFS: gcm-client.c:525:2: error: implicit declaration of function
      'gdk_drawable_get_size'. (Closes: #622006)
  * Bump Standards-Version to 3.9.2. No further changes.

 -- Michael Biebl <biebl@debian.org>  Wed, 27 Apr 2011 00:42:57 +0200

gnome-color-manager (2.30.2-1) unstable; urgency=low

  * New upstream bugfix release.
  * debian/watch
    - Only track stable releases.
  * Switch to source format 3.0 (quilt)
    - Add debian/source/format.
  * Add gnome-pkg-tools integration (shamlessly copied from d-conf)
    - Rename debian/control to debian/control.in.
    - Set Uploaders to @GNOME_TEAM@.
    - Include /usr/share/gnome-pkg-tools/1/rules/uploaders.mk and override the
      clean target for an up-to-date Uploaders list.
    - Include /usr/share/gnome-pkg-tools/1/rules/gnome-get-source.mk and set
      GNOME_MODULE to gnome-color-manager for the get-orig-source target.
  * Bump Standards-Version to 3.9.0. No further changes.

 -- Michael Biebl <biebl@debian.org>  Sun, 04 Jul 2010 15:56:49 +0200

gnome-color-manager (2.30.1-1) unstable; urgency=low

  [ Roland Mas ]
  * Added new build-depends.

  [ Michael Biebl ]
  * New upstream release.
  * debian/control
    - Wrap Build-Depends.
    - Add Vcs-* fields.
    - Update Build-Depends with the version requirements specified in
      configure.ac.
    - Add Build-Depends on libx11-dev.
    - Add Build-Depends on libnotify-dev.
    - Add Build-Depends on libsane-dev.

 -- Roland Mas <lolando@debian.org>  Tue, 02 Mar 2010 15:11:21 +0100

gnome-color-manager (2.29.2-1) experimental; urgency=low

  * Initial packaging, just a tiny bit of dh7 (closes: #566884).

 -- Roland Mas <lolando@debian.org>  Mon, 25 Jan 2010 20:45:10 +0100
